import email
import email.header
from enum import Enum
import os
import quopri
import re

from imapidle import imaplib

import secrets
import source


class Information(Enum):
    required = 0
    optional = 1

class Update(Enum):
    edit = 0
    create = 1
    remove = 2
    rename = 3
    attach = 4
    delete_attach = 5

DEFAULT_SENDER = 'wiki@crans.org'

UPDATES = (
	# fonction de pré-traitement du mail
	lambda mail: quopri.decodestring(mail.get_payload()).decode('utf-8').replace('\r', ''), {

		Update.edit: (
			lambda **kwargs: '\x0310{page}\x03 \x033{user}\x03 {comment}\x03 \x0315(r{rev})\x03'.format(**kwargs) if kwargs['comment']\
						else '\x0310{page}\x03 \x033{user}\x03 \x0315(r{rev})\x03'.format(**kwargs),
			'page', [
				(Information.required, ['page', 'user'], r'La page « (?P<page>.*) » a été modifiée par (?P<user>.*) :'),
				(Information.required, ['rev'], r'\?action=diff&rev1=[0-9]+&rev2=(?P<rev>[0-9]+)\n'),
				(Information.optional, ['comment'], r'\s*Commentaire :\s*\n\s*(?P<comment>[^\n]*)\n')
			]
		),

		Update.create: (
			lambda **kwargs: '\x0310{page}\x03 \x033{user}\x03 {comment}\x03 \x0315(r1)\x03'.format(**kwargs) if kwargs['comment']\
						else '\x0310{page}\x03 \x033{user}\x03 \x0315(r1)\x03'.format(**kwargs),
			'page', [
				(Information.required, ['page', 'user'], r'La page « (?P<page>.*) » a été modifiée par (?P<user>.*) :'),
				(Information.required, [], r'\s*Nouvelle page :\n'),
				(Information.optional, ['comment'], r'\s*Commentaire :\s*\n\s*(?P<comment>[^\n]*)\n')
			]
		),

		Update.remove: (
			lambda **kwargs: '\x0310{page}\x03 \x033{user}\x03 {comment}\x03'.format(**kwargs) if kwargs['comment']\
						else '\x0310{page}\x03 \x033{user}\x03'.format(**kwargs),
			'page', [
				(Information.required, ['page', 'user'], r'La page « (?P<page>.*) » a été supprimée par (?P<user>.*) :'),
				(Information.optional, ['comment'], r'\s*Commentaire :\s*\n\s*(?P<comment>[^\n]*)\n')
			]
		),

		Update.rename: (
			lambda **kwargs: '\x0310{page}\x03 \x033{user}\x03 rename --> \x0310{new}\x03 {comment}\x03'.format(**kwargs) if kwargs['comment']\
						else '\x0310{page}\x03 \x033{user}\x03 rename --> \x0310{new}\x03'.format(**kwargs),
			'page', [
				(Information.required, ['page', 'new', 'user'], r'La page « (?P<page>.*) » a été renommée « (?P<new>.*) » par (?P<user>.*) :'),
				(Information.optional, ['comment'], r'\s*Commentaire :\s*\n\s*(?P<comment>[^\n]*)\n')
			]
		),

		Update.attach: (
			lambda **kwargs: '\x0310{page}\x03 \x033{user}\x03 ajout de la pièce jointe : \x0310{name}\x03'.format(**kwargs),
			'page', [
				(Information.required, ['page', 'user'], r'vous vous êtes abonné aux notifications de changements pour la page "(?P<page>.*)"\.Une pièce jointe vient d\'y être ajouté par (?P<user>.*)\. Quelques détails sur la pièce jointe :'),
				(Information.required, ['name'], r'Nom\s*:\s*(?P<name>[^\n]*)\n')
			]
		),

		Update.delete_attach:(
			lambda **kwargs: '\x0310{page}\x03 \x033{user}\x03 suppression de la pièce jointe : \x0310{name}\x03'.format(**kwargs),
			'page', [
				(Information.required, ['page', 'user'], r'vous vous êtes abonné aux notifications de changements pour la page "(?P<page>.*)"\.Une pièce jointe de cette page vient d\'être supprimée par (?P<user>.*)\. Quelques détails sur la pièce jointe :'),
				(Information.required, ['name'], r'Nom\s*:\s*(?P<name>[^\n]*)\n')
			]
		)
	}
)

class Wiki(source.Source):
	def __init__(self):
		source.Source.__init__(self, 'wiki', 'wiki@crans.org')

	def parse_mail(self, mail):
		sender = mail['From']
		preprocessor, updates = UPDATES
		try:
			body = preprocessor(mail)
		except:
			return
		for update in updates:
			next_update = False
			message, page, regexps = updates[update]
			kwargs = {}
			for info, groups, regexp in regexps:
				m = re.search(regexp, body)
				if info == Information.required and not m:
					next_update = True
					break
				elif m:
					for group in groups:
						kwargs[group] = m.group(group)
				else:
					for group in groups:
						kwargs[group] = None
			if next_update: continue
			message = message(**kwargs)
			page = kwargs[page]
			self.push_update(page, message)
