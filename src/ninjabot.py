import json
import os
import re
import select
import threading
import time

from bot import Bot
import config
from subs import subs


def write_subs(subs):
	with open('subs.py', 'w') as s:
		s.write('subs={' + ','.join([repr(source)+':{' + ','.join([repr(nick) + ':{' + ','.join([repr(channel) + ':[' + ','.join([repr(patt) for patt in subs[source][nick][channel]]) + ']' for channel in subs[source][nick]]) + '}' for nick in subs[source]]) + '}' for source in subs]) + ' }\n')

def subscribed(channel):
    global subs
    for source in subs:
         for nick in subs[source]:
             if channel in subs[source][nick]:
                 return True
    return False


class Ninja(Bot):
	def __init__(self):
		Bot.__init__(self, "NinjaBot")
		self.sources = []

	def on_welcome_ext(self, conn, e):
		conn.join('#wikistalk')
		channels = set()
		for source in subs:
			for nick in subs[source]:
				for channel in subs[source][nick]:
					if channel[0] in config.channel_start:
						channels.add(channel)
		for channel in channels:
			conn.join(channel)
		self.thread = threading.Thread(target=self.loop)
		self.thread.start()

	def loop(self):
		try:
			os.remove(config.socket_path)
		except OSError:
			pass
		os.mkfifo(config.socket_path)
		while True:
			fifo = open(config.socket_path)
			data = fifo.read()
			fifo.close()
			try:
				if data.startswith('SOURCES '):
					self.sources = data[8:].split(',')
					print('Sources are now :', data[8:])
				else:
					data = json.loads(data)
					source = data['source']
					page = data['page']
					message = data['message']
					self.push_update(source, page, message)
			except:
				continue

	def push_update(self, source, page, message):
		global subs
		patts = {}
		for nick in subs[source]:
			for channel in subs[source][nick]:
				if channel not in patts: patts[channel] = []
				patts[channel] += subs[source][nick][channel]
		for channel in patts:
			if any(re.fullmatch(patt, page) for patt in patts[channel]):
				t = time.time()
				self.connection.privmsg(channel, message)
				# on attend 5 secondes pour éviter que le serveur nous empêche d'envoyer les messages
				# time.sleep n'a pas l'air de trop marcher dans les thread du coup je le combine avec time.time
				while t + 5 > time.time():
					time.sleep(1)

	def do_command_ext(self, conn, command, level, nick):
		global subs
		for source in self.sources:
			if source not in subs:
				subs[source] = {}
			if nick not in subs[source]:
				subs[source][nick] = {}
		if command[0].casefold() == "help":
			conn.privmsg(nick, "Sources : " + ", ".join(self.sources))
			conn.privmsg(nick, "Commandes (arguments optionnels entre []) :")
			conn.privmsg(nick, " - sub source (#channel|yournick) regex : abonner #channel ou soi-même aux modifications sur les pages matchant regex (au format python).")
			conn.privmsg(nick, " - list [source] : lister ses abonnements.")
			conn.privmsg(nick, " - del [source] [#channel|yournick] [n] : supprimer le nième abonnement donné par list.")
			if level >= 100:
				conn.privmsg(nick, " - listall : lister tous les abonnements.")
			if level >= float('inf'):
				conn.privmsg(nick, " - sudo nick command : exécuter la commande en tant que nick (attention : les éventuels messages privés de réponse seront envoyés à cette personne).")
		elif command[0].casefold() == "sub":
			if len(command) >= 4:
				patt = ' '.join(command[3:])
				correct_source = False
				for source in self.sources:
					if source == command[1]:
						correct_source = True
						break
				if not correct_source:
					conn.privmsg(nick, "Source {source} incorrecte".format(source=command[1]))
					return
				channel = command[2] if command[2][0] in config.channel_start else nick
				if channel not in subs[source][nick]:
					subs[source][nick][channel] = []
					if channel[0] in config.channel_start:
						conn.join(channel)
				subs[source][nick][channel].append(patt)
				conn.privmsg(nick, "Vous avez abonné {channel} à {patt} sur {source}".format(channel=channel, patt=patt, source=command[1]))
		elif command[0].casefold() == "del":
			if len(command) == 1:
				channels = set()
				for source in subs:
					for channel in subs[source][nick]:
						if channel[0] in config.channel_start:
							channels.add(channel)
					subs[source][nick] = {}
				conn.privmsg(nick, "Tous les abonnements ont été supprimés")
				for channel in channels:
					if not subscribed(channel):
						conn.part(channel, "Plus d'abonnement sur {channel}".format(channel=channel))
				return
			correct_source = False
			for source in self.sources:
				if source == command[1]:
					correct_source = True
					break
			if not correct_source:
				conn.privmsg(nick, "Source {source} incorrecte".format(source=command[1]))
				return
			elif len(command) == 2:
				channels = { channel for channel in subs[source][nick] if channel[0] in config.channel_start }
				subs[source][nick] = {}
				conn.privmsg(nick, "Tous les abonnments provenant de {source} ont été supprimés".format(source=command[1]))
				for channel in channels:
					if not subscribed(channel):
						conn.part(channel, "Plus d'abonnement sur {channel}".format(channel=channel))
				return
			channel = command[2] if command[2][0] in config.channel_start else nick
			if channel not in subs[source][nick]:
				conn.privmsg(nick, "Vous n'avez pas d'abonnement à {source} sur {channel}".format(source=command[1], channel=channel))
				return
			elif len(command) == 3:
				if channel in subs[source][nick]:
					del subs[source][nick][channel]
				conn.privmsg(nick, "Vos abonnements à {source} sur {channel} on été supprimés".format(source=command[1], channel=channel))
				if channel[0] in config.channel_start:
					if not subscribed(channel):
						conn.part(channel, "Plus d'abonnement sur {channel}".format(channel=channel))
			elif len(command) == 4:
				if command[3].isnumeric():
					n = int(command[3])
					if n < len(subs[source][nick][channel]):
						subscription = subs[source][nick][channel][n]
						del subs[source][nick][channel][n]
						conn.privmsg(nick, "Votre abonnement, {subscription}, à {source} sur {channel} a été supprimé".format(subscription=subscription, source=command[1], channel=channel))
						if len(subs[source][nick][channel]) == 0:
							del subs[source][nick][channel]
							if channel[0] in config.channel_start:
								if not subscribed(channel):
									conn.part(channel, "Plus d'abonnement sur {channel}".format(channel=channel))
		elif command[0].casefold() == "list":
			if len(command) == 1:
				for source in subs:
					conn.privmsg(nick, source + ' :')
					for channel in subs[source][nick]:
						conn.privmsg(nick, ' - ' + channel + ' :')
						for i, patt in enumerate(subs[source][nick][channel]):
							 conn.privmsg(nick, '   + {i}. {patt}'.format(i=i, patt=patt))
				return
			correct_source = False
			for source in self.sources:
				if source == command[1]:
					correct_source = True
					break
			if not correct_source:
				conn.privmsg(nick, "Source {source} incorrecte".format(source=command[1]))
				return
			if len(command) == 2:
				for channel in subs[source][nick]:
					conn.privmsg(nick, channel + ' :')
					for i, patt in enumerate(subs[source][nick][channel]):
						 conn.privmsg(nick, ' - {i}. {patt}'.format(i=i, patt=patt))
		elif command[0].casefold() == "sudo":
			if len(command) >= 3 and level == float('inf'):
				self.do_command_ext(conn, command[2:], 0, command[1])
		elif command[0].casefold() == "listall" and level >= 100:
			for source in subs:
				conn.privmsg(nick, source + ':')
				for n in subs[source]:
					conn.privmsg(nick, ' - ' + n + ':')
					for channel in subs[source][n]:
						conn.privmsg(nick, '   + ' + channel)
						for i, patt in enumerate(subs[source][n][channel]):
							conn.privmsg(nick, '     * {i}. {patt}'.format(i=i, patt=patt))
		write_subs(subs)


if __name__ == "__main__":
	ninja = Ninja()
	ninja.start()
