import base64
import email
import email.header
import quopri
import re

import html2text
from imapidle import imaplib

import secrets
import source


def decode_header(header):
	header_parts = email.header.decode_header(header)
	header = ''
	for part, encoding in header_parts:
		if isinstance(part, str):
			header += part
		else:
			header += part.decode('ascii' if encoding is None else encoding, 'ignore')
	return header


class Phabricator(source.Source):
	def __init__(self):
		source.Source.__init__(self, 'phabricator', 'noreply@phabricator.crans.org')

	def parse_mail(self, mail):
		sender = decode_header(mail['From'])
		if self.address not in sender:
		    return
		try:
			projects = [decode_header(project) for project in mail['X-Phabricator-Projects'].split(',')]
			projects = [project.strip()[2:-1] for project in projects]
		except:
			projects = []
		subject = decode_header(mail['Subject'])
		tags = []
		for m in re.finditer(r'\[(?P<tag>[^]]*)\]', subject):
			tags.append(m.group('tag'))
		m = re.search(r'(?P<task>T\d+):\s+(?P<name>.*)$', subject)
		if m is None:
			print("No task")
			return
		task_id = task = m.group('task')
		if projects:
			print(projects[0])
			task = projects[0] + '/' + task
		task_name = m.group('name')
		m = re.search(r'"?(?P<author>[^"<]*)"?\s*<', sender)
		if m is None:
			print("No author:", sender)
			return
		author = m.group('author')
		self.push_update(task, '\x0310{task}\x03 \x033{author}\x03 \x02{tag}\x02\x03 \x0315{name}\x03'.format(task=task_id, author=author, tag=tags[1], name=task_name))
