import datetime
import nntplib
import time

import source


class News(source.Source):
	def __init__(self):
		source.Source.__init__(self, 'news', 'news.crans.org')

	def loop(self):
		host = 'news.crans.org'
		port = 119
		nntp = nntplib.NNTP(host, port)
		groups = nntp.newgroups(datetime.date(1970, 1, 1))[1]
		posts = { group.group : int(group.last) for group in groups }
		while True:
			time.sleep(5)
			try:
				groups = nntp.newgroups(datetime.date(1970, 1, 1))[1]
				for group in groups:
					nntp.group(group.group)
					if not group.group in posts:
						posts[group.group] = 1
					for i in range(posts[group.group]+1, int(group.last)+1):
						try:
							article = nntp.head(i)[1].lines
							author = subject = ''
							for j in range(len(article)):
								line = article[j]
								k = j + 1
								while k < len(article):
									if article[k][0] != 32:
										break
									line += article[k]
									k += 1
								line = line.decode('utf-8', 'ignore')
								if line.startswith('From: '):
									author = line[6:]
								if line.startswith('Subject: '):
									subject = email.header.decode_header(line[9:])[0]
									if subject[1] is None:
										subject = subject[0]
									else:
										subject = subject[0].decode(subject[1])
							self.push_update(Source.News, group.group, '\x0310{group}\x03 \x033{author}\x03  {subject}\x03'.format(group=group.group, author=author, subject=subject))
						except:
							continue
				posts = { group.group : int(group.last) for group in groups }
			except:
				nntp = nntplib.NNTP(host, port)