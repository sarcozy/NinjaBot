import base64
import email
import email.header
import quopri
import re

import html2text
from imapidle import imaplib

import secrets
import source


def decode_header(header):
	header_parts = email.header.decode_header(header)
	header = ''
	for part, encoding in header_parts:
		if isinstance(part, str):
			header += part
		else:
			header += part.decode('ascii' if encoding is None else encoding, 'ignore')
	return header


class Github(source.Source):
	def __init__(self):
		source.Source.__init__(self, 'github', 'notifications@github.com')

	def parse_mail(self, mail):
		sender = decode_header(mail['From'])
		if self.address not in sender:
		    return
		subject = decode_header(mail['Subject'])
		m = re.match(r"^\[(?P<repository>[^]]*)\] (?P<release>.*)$", subject)
		repo = m.group('repository')
		release = m.group('release')
		self.push_update(repo, "\x0310https://github.com/{repo}\x0F \x0303{name}\x0F".format(repo=repo, name=release))
