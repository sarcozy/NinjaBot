import email
import email.header
import json
import quopri
import threading

from imapidle import imaplib


import config
import secrets


class Source:
	def __init__(self, name, address=None, args=(), kwargs={}):
		self.name = name.casefold()
		self.address = address.casefold()
		self.thread = threading.Thread(target=self.loop, args=args, kwargs=kwargs)
		self.thread.start()

	def push_update(self, page, message):
		with open(config.socket_path, 'w') as fifo:
			fifo.write(json.dumps({'source': self.name, 'page': page, 'message': message}, separators=(',', ':')))

	def loop(self, *args, **kwargs):
		return

class MailSources:
	def __init__(self, sources=None):
		self.name = ''
		self.sources = sources if sources else []
		self.thread = threading.Thread(target=self.loop)
		self.thread.start()

	def loop(self):
		while True:
			try:
				imap_client = imaplib.IMAP4_SSL(host=secrets.host)
				imap_client.login(secrets.user, secrets.password)
				imap_client.select()
				print('email: Running...')
				while True:
					for uid, msg in imap_client.idle():
						if msg == b'EXISTS':
							fetcher = imaplib.IMAP4_SSL(host=secrets.host)
							fetcher.login(secrets.user, secrets.password)
							fetcher.select()
							mail = fetcher.fetch(uid.decode('ascii'), '(RFC822)')
							fetcher.close()
							fetcher.logout()
							mail = email.message_from_bytes(mail[1][0][1])
							try: print('Received new mail from', mail['From'])
							except: pass
							for source in self.sources:
								try:
									source.parse_mail(mail)
								except Exception as e:
									print(e)
									continue
			except Exception as e:
				print(e)
				continue
